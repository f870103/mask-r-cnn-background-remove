import cv2 as cv
from PIL import Image

if __name__ == "__main__":
    image = cv.imread('enoden.jpg', -1)
    cv.imshow('Fig A', image)

    image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
    pil = Image.fromarray(image)
    pil.save('enoden_pil.jpg')

    while True:
        if cv.waitKey(1) & 0xFF == ord('q'):
            cv.destroyAllWindows()
            break;
