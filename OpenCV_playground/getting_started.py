import numpy as np
import cv2 as cv

def read_image_and_average ():
    img = cv.imread('enoden.jpg', cv.IMREAD_UNCHANGED)
    kernel = np.ones((5, 5), np.float32)/25
    dst = cv.filter2D(img, -1, kernel)

    cv.imshow('Figure 1', img)
    cv.imshow('Figure 2', dst)

    key = cv.waitKey(0)

    if key == 27:   # ESC
        cv.destroyAllWindows()
    elif key == ord('s'):
        cv.imwrite('averaged_enoden.jpg', dst)
        cv.destroyAllWindows()

def read_video ():
    print (cv.__version__)

    cap = cv.VideoCapture('test_vid.mp4')
    fps = cap.get(cv.CAP_PROP_FPS)
    width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
    fourcc = cv.VideoWriter_fourcc(*'mp4v')
    writer = cv.VideoWriter()
    writer.open(filename='output.avi', fourcc=fourcc, fps=fps, frameSize=(width, height), isColor=True)

    if cap.isOpened():
        print('File opened, proceed process')
    else:
        print('Error occurred')

    while cap.isOpened() and writer.isOpened():
        ret, frame = cap.read()
        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        gray = cv.cvtColor(gray, cv.COLOR_GRAY2RGB)
        gray_with_fps = cv.putText(gray, 'FPS=' + str(format(fps)), (0, 100), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255))
        writer.write(gray_with_fps)
        cv.imshow('frame', gray_with_fps)
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    writer.release()
    cv.destroyAllWindows()


if __name__ == '__main__':
    read_video()
