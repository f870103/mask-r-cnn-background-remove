import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

def capture_camera():
    cap = cv.VideoCapture(0)
    if not cap.isOpened():
        print("cannot open camera.")
        exit()

    while True:
        # read the video frame by frame, if the frame is read correctly ret == true
        ret, frame = cap.read()

        if not ret:
            print("Can't receive frame (stream end?). exit.")
            break

        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        cv.imshow('image', gray)
        cv.imshow('image2', frame)

        if cv.waitKey(1) == 27: # ESC
            break

    # release the capture
    cap.release()
    cv.destroyAllWindows()

if __name__ == '__main__':
    capture_camera()
